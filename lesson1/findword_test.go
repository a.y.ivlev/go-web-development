package main

import (
	"testing"
)

func TestFindWord(t *testing.T) {
	cases := map[string]struct{
		fword			string
		url				string
		want			bool
		errorMessage	string
	}{
		"Test 1": {
			fword:  		"Brunei Darussalam",
			url: 			"https://restcountries.eu/rest/v2/name/rus",
			want:			true,
		},
		"Test 2": {
			fword:  		"погода",
			url: 			"https://mail.ru/",
			want: 			true,
		},
		"Test 3": {
			fword:  		"Estonia",
			url: 			"https://restcountries.eu/rest/v2/name/eesti",
			want: 			true,
		},
		"Test 4": {
			fword:  		"Brunei Darussalam",
			url: 			"https://restcountries.eu/rest/v2/name/eesti",
			want: 			false,
			errorMessage:	"по данному URL https://restcountries.eu/rest/v2/name/eesti искомое слово Brunei Darussalam не найдено.\n",
		},
		"Test 5": {
			fword:  		"погода",
			url: 			"mailcom",
			want: 			false,
			errorMessage:	"ошибка GET запроса не корректный URL mailcom .\n",
		},
	}

	for name, cs := range cases {
		t.Run(name, func(t *testing.T){
			result, err := findString(cs.fword, cs.url)
				if cs.want != result {
					t.Errorf("expected: %t, actual: %t", cs.want, result)
				}
				if err != nil {
					if err.Error() != cs.errorMessage {
						t.Errorf("expected err: %s, actual err: %s", cs.errorMessage, err.Error())
					}
				}
		})
	}
}
