// Программа для поиска точного соответствия фразе заданного выражения в тексте ответа от сервера
// по каждому из указанных URL. Результат работы программы выводиться в консоль, а также URL
// страницы где был обнаружен поисковый запрос, записывается в слайс.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"net/http"
	"regexp"
	"strings"
)

func main() {
	links := make([]string, 0, 10)
	var fString string
	flag.StringVar(&fString, "w", "", "после флага -w фраза для поиска задаётся в кавычках, затем указывается один или несколько URL адресов.")
	flag.Parse()
	if fString == "" {
		fmt.Println("не установлен флаг -w и не задана фраза для поиска.")
	}
	for _, url := range flag.Args() {
		if !strings.HasPrefix(url, "http://") && !strings.HasPrefix(url, "https://") {
			url = "http://" + url
		}
		result, err := findString(fString, url)
		if err != nil {
			fmt.Println(err)
		}
		if result {
			fmt.Printf("Искомя фраза \"%s\" найдена по адресу %s .\n", fString, url)
			links = append(links, url)
		}
	}
}

// Функция findString принимает в качестве входных параметров искомое выражение и URL,
// по которому будет осуществляется поиск. Делает GET запрос и производит поиск в ответе
// от сервера искомого выражения. Результатом работы функции являетя bool и ошибка.
func findString(fString string, url string) (bool, error) {
	resp, err := http.Get(url)
	if err != nil {
		err = fmt.Errorf("ошибка GET запроса не корректный URL %s .\n", url)
		return false, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("ошибка получения ответа от %s: %s\n", url, resp.Status)
		return false, err
	}
	input := bufio.NewScanner(resp.Body)
	input.Scan()
	var re = regexp.MustCompile(fString)
	str := re.FindString(input.Text())
	if str == "" {
		err = fmt.Errorf("по данному URL %s искомое слово %s не найдено.\n", url, fString)
		return false, err
	}
	return true, nil
}
